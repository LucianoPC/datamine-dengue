require 'csv'

namespace :import do
  desc "Import a CSV file"
  task csv_file: :environment do
    csv_text = File.read('lib/assets/series_historicas.csv')
    csv = CSV.parse(csv_text, :headers => true)

    print "Creating States: "

    csv.each do |row|
      row = row.to_s.split(';')

      state = State.create(name: row[0])

      row.shift
      row.each_with_index do |element, i|
        state.periods << Period.create(year: (1990 + i), number: element)
      end

      print '.'
    end

    puts ' '
  end

end