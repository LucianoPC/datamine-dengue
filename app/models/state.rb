class State < ActiveRecord::Base

  ACRONYM = [
    "Acre",
    "Alagoas",
    "Amapa",
    "Amazonas",
    "Bahia",
    "Ceara",
    "Distrito Federal",
    "Espirito Santo",
    "Goias",
    "Maranhao",
    "Mato Grosso",
    "Mato Grosso do Sul",
    "Minas Gerais",
    "Para",
    "Paraiba",
    "Parana",
    "Pernambuco",
    "Piaui",
    "Rio Grande do Norte",
    "Rio Grande do Sul",
    "Rio de Janeiro",
    "Rondonia",
    "Roraima",
    "Santa Catarina",
    "Sao Paulo",
    "Sergipe",
    "Tocantins"
  ]

  has_many :periods, dependent: :destroy

  def <=> (other)
    self.name <=> other.name
  end

  def number year
    Period.find_by(state_id: self.id, year: year).number
  end

  def chart_formated
    hash_chart = {}

    self.periods.each do |period|
      hash_chart[period.year.to_s] = period.number
    end

    hash_chart
  end
end
