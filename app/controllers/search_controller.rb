class SearchController < ApplicationController

  def index
    @state = State.find_by_name(params[:name])
    @selected = @state ? @state.name : ""
  end
end
