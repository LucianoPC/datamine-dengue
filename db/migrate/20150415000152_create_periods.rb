class CreatePeriods < ActiveRecord::Migration
  def change
    create_table :periods do |t|
      t.integer :year
      t.integer :number
      t.belongs_to :state, index: true

      t.timestamps null: false
    end
  end
end
